package com.xetree.yourtube;

/**
 * Created by Farhan on 9/21/2016.
 */

    import android.content.Context;
    import android.content.Intent;
    import android.support.v7.widget.PopupMenu;
    import android.support.v7.widget.RecyclerView;
    import android.view.LayoutInflater;
    import android.view.MenuItem;
    import android.view.View;
    import android.view.ViewGroup;
    import android.widget.ImageView;
    import android.widget.TextView;

    import com.xetree.yourtube.R;
    import com.xetree.yourtube.model.VideoCategory;
    import com.bumptech.glide.Glide;

    import java.util.List;

/**
 * Created by Ravi Tamada on 18/05/16.
 */
public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.MyViewHolder> {

    private Context mContext;
    private List<VideoCategory> categoriesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        public ImageView thumbnail, overflow;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            count = (TextView) view.findViewById(R.id.count);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            //overflow = (ImageView) view.findViewById(R.id.overflow);
        }
    }

    public CategoriesAdapter(Context mContext, List<VideoCategory> categoriesList) {
        this.mContext = mContext;
        this.categoriesList = categoriesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final VideoCategory category = categoriesList.get(position);
        holder.title.setText(category.getTitle());


        // loading album cover using Glide library
        if(category.getImageUrl()==null)
        {
            Glide.with(mContext).load(category.getThumbnail()).placeholder(R.drawable.category_placeholder).into(holder.thumbnail);
        }
        else {
            Glide.with(mContext).load(category.getImageUrl()).placeholder(R.drawable.category_placeholder).into(holder.thumbnail);
        }

        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(mContext,YouTubeActivity.class);
                i.putExtra("category_playlist_id",category.getPlayListId());
                i.putExtra("category_playlist_title",category.getTitle());

                mContext.startActivity(i);
            }
        });

        /*
        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPopupMenu(holder.overflow);
            }
        });
        */
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view) {

        // inflate menu
        /*
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_category, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
        popup.show();
        */
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            /*
            switch (menuItem.getItemId()) {

                case R.id.action_add_favourite:
                    //Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();

                    return true;

                case R.id.action_play_next:
                    Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
                    return true;

                default:
            }
            */
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
    }
}