package com.xetree.yourtube.model;

/**
 * Created by Farhan on 9/23/2016.
 */
public class MyVideo {
    private int key_id;
    private String id;
    private String title;
    private String viewCount;
    private String duration;
    private String thumbnailUrl;

    public MyVideo() {

    }

    public int getKey_id() {
        return key_id;
    }

    public void setKey_id(int key_id) {
        this.key_id = key_id;
    }

    public MyVideo(String id, String title, String viewCount, String duration, String thumbnailUrl) {
        this.id = id;
        this.title = title;
        this.viewCount = viewCount;
        this.duration = duration;
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getViewCount() {
        return viewCount;
    }

    public void setViewCount(String viewCount) {
        this.viewCount = viewCount;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
}
